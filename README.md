# CodeDiscussions

This repository contains supplementary material for the weekly discussions on code development in the hadron physics division at Uppsala University.

## Format

Each presentation, including discussion, should take no more than 15 to 20 minutes. The following points should be considered for the preparation.

- What is it?
- Pros and Cons
- General example
- Specific example, relating to our work (if possible)
- Suggestion of a new topic

## Upcoming topics

- gcc vs clang, general introduction (Adeel)
- pointers / smart pointers (Walter)

## Unassigned topics

- von Neumann bottleneck

## Past topics

- constexpr (Jenny)