
#include <iostream>       // std::cin, std::cout
#include <queue>          // std::queue

int main ()
{

  // queue::push/pop
  
  std::queue<int> myqueue;
  int myint;

  std::cout << "Please enter some integers (enter 0 to end):\n";

  do {
    std::cin >> myint;
    myqueue.push (myint);
  } while (myint);

  std::cout << "myqueue contains: ";
  //while (!myqueue.empty())
  //{
    std::cout << ' ' << myqueue.front();
    //myqueue.pop();
  //}

  myqueue.pop();

  std::cout << ' ' << myqueue.front();

  std::cout << '\n';

  // stack::push/pop second example

  std::queue<int> anotherqueue;

  for (int i=0; i<5; ++i) anotherqueue.push(i);

  std::cout << "Popping out elements...";
  while (!anotherqueue.empty())
  {
     std::cout << ' ' << anotherqueue.front();
     anotherqueue.pop();
  }
  std::cout << '\n';

  for (int i=0; i<5; ++i) anotherqueue.push(i);

  std::cout << "Popping out elements...";
  while (!anotherqueue.empty())
  {
     std::cout << ' ' << anotherqueue.back();
     anotherqueue.pop();
  }
  std::cout << '\n';

  // queue::front

  std::queue<int> mysecondqueue;

  mysecondqueue.push(77);
  mysecondqueue.push(16);

  mysecondqueue.front() -= mysecondqueue.back();    // 77-16=61

  std::cout << "mysecondqueue.front() is now " << mysecondqueue.front() << '\n';

  // queue::back

  std::queue<int> mythirdqueue;

  mythirdqueue.push(12);
  mythirdqueue.push(75);   // this is now the back

  mythirdqueue.back() -= mythirdqueue.front();

  std::cout << "mythirdqueue.back() is now " << mythirdqueue.back() << '\n';

  return 0;
}


